\beamer@endinputifotherversion {3.33pt}
\select@language {ngerman}
\beamer@sectionintoc {1}{Einleitung}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{DPD und MPCD}{5}{0}{1}
\beamer@sectionintoc {2}{DPD: Dissipative Particle Dynamics}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Der Algorithmus f\IeC {\"u}r ein isothermes Fluid}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Eine einfache Anwendung: Polymer in einem Fluid}{16}{0}{2}
\beamer@subsectionintoc {2}{3}{Aktuelle Forschung: WBC Margination}{20}{0}{2}
\beamer@sectionintoc {3}{MPCD: Multi Particle Collision Dynamics}{24}{0}{3}
\beamer@subsectionintoc {3}{1}{Aufbau und Algorithmus der Simulationsmethode}{25}{0}{3}
\beamer@subsectionintoc {3}{2}{Die Randbedingungen: Ghost-Particles}{32}{0}{3}
\beamer@subsectionintoc {3}{3}{Simulation von RBCs in einem Kapillar}{35}{0}{3}
\beamer@sectionintoc {4}{Ausblick}{44}{0}{4}
