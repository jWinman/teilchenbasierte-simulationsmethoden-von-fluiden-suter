\input{headerprak.tex}
\begin{document}
\begin{titlepage}
\subject{Seminar: Hydrodynamik des Blutes}
\title{\Huge{Teilchenbasierte Simulationsmethoden von FluideN}}
\date{Vorgetragen am 12.06.2014}
\author{Jens Winkelmann \\ \footnotesize{jens.winkelmann@tu-dortmund.de}}
\publishers{
\dictum[Sepp Blatter]{
"`Soviel Geld lässt sich, weiß Gott, nicht mit etwas Gutem verdienen."'
}
}
\end{titlepage}
\maketitle

%\tableofcontents
\newpage
\paragraph{Einleitung:}
In diesem Vortrag wurden die beiden teilchenbasierten Simulationsmethoden Dissipative-Particle \cite{koelman} und Multiparticle Collision Dynamics \cite{kapral} vorgestellt, mit denen Fluide auf mesoskopischen Skalen simuliert werden können.
Beide Methoden sind sogenannte Coarse-Grained Methoden, die nicht jedes einzelne Fluidmolekül selbst simulieren, sondern eine Gruppe von Molekülen, deren Schwerpunkt dann durch ein DPD- bzw. MPCD-Teilchen repräsentiert werden.

\paragraph{DPD: Dissipative Partice Dynamics:}
Bei der DPD-Methode wird für jedes Fluidteilchen $i$ ähnlich wie bei einer MD-Simulation eine Bewegungsgleichung aufgestellt,
\begin{align*}
\frac{\dx \vec{r}_i}{\dx t} &= \vec{v}_i \, ,\\
\frac{\dx \vec{v}_i}{\dx t} &= \frac{1}{m} \sum_{j \neq i} (\vec{F}_{ij}^C + \vec{F}_{ij}^D + \vec{F}_{ij}^R)\,, \quad \vec{F}_{ij} = \vec{F}_i - \vec{F}_j
\end{align*}
mit den drei Kräften 
\begin{align*}
\text{konservativ:} \quad \frac{1}{m} \vec{F}_{ij}^C &= -\frac{1}{m} \frac{\partial \phi(r_{ij})}{\partial \vec{r}_{ij}} \,, \\ %= a w_C(r_{ij}) \vec{\hat{r}}_{ij}} \\
\text{dissipativ:} \quad \frac{1}{m} \vec{F}_{ij}^D &= - \gamma w_D(r_{ij}) (\vec{\hat{r}}_{ij} \cdot \vec{v}_{ij}) \vec{\hat{r}}_{ij} \, \\
\text{zufällig:} \quad \frac{1}{m} \vec{F}_{ij}^R & = \sigma w_R (r_{ij}) \xi_{ij} \, \vec{\hat{r}}_{ij}\, ,\\
\text{mit} &\avg{(\xi_{ij})_t} = 0 \quad \avg{(\xi_{ij})_t (\xi_{lk})_{t'}} = \frac{1}{\Updelta t}(\updelta_{il}\updelta_{jk} + \updelta_{ik}\updelta_{jl})\updelta_{tt'}\, .
\end{align*}
$\vec{\hat{r}}_{ij}$ sind dabei die örtlichen Einheitsvektoren zwischen den beiden Teilchen $i$ und $j$.
Die konservative und somit energieerhaltende Kraft $\vec{F}^C$ leitet sich aus einem Potential $\phi$ ab. \\
Die dissipative und zufällige Kraft bestehen beide aus einer Gewichtsfunktion $w_D$ bzw $w_R$ mit einem Cutoff-Radius $r_c$ und einem Stärkefaktor $\gamma$ bzw. $\sigma$, die beide über das Dissipations-Fluktuationstheorem verbunden sind.
Die dissipative Kraft, die von der Geschwindigkeit $\vec{v}$ abhängt, kann dabei als eine viskose, bremsende Kraft zwischen zwei Teilchen interpretiert werden.
Bewegen sich zwei Teilchen aufeinander zu / von einander weg, so ist das Vorzeichen der Kraft positiv/negativ und wirkt der Bewegung somit immer entgegen.
Zusätzlich besteht die Zufallskraft aus einem Zufallsfaktor $\xi$, der in der diskretisierten Darstellung die beiden angegebenen Momente besitzt.
Aufgrund der Impulserhaltung muss dabei die Zufallszahl unter Teilchenverstauschung von $i$ und $j$ die gleiche sein.
Die Kopplung eines Objektes in eine DPD Flüssigkeit geschieht über die konservative Kraft.
Hierbei wird meistens eine weiche, repulsive und rein phänomenologische Kraft gewählt, die ebenfalls proportional zu der Gewichtsfunktion $w_R$ ist.
Ebenso lässt sich über diese Kraft die Art des Lösungsmittels einstellen.
Ist die Kraft zwischen den Lösungsmittelteilchen kleiner als die zwischen Lösungsmittel- und gelösten Teilchen, so handelt es sich dabei um ein schlechtes Lösungsmittel, da die Lösungsmittelteilchen sich gegenseitig weniger abstoßen. \\
Eine Anwendung der DPD-Methode wurde erst neulich in einer Veröffentlichung des Forschungszentrum Jülich gezeigt, bei der die WBC (White Blood Cells) Margination, die tendenzielle Bewegung der Leukozyten in Richtung der Gefäßwand, simuliert wurde \cite{fedosov14}.
Die roten sowie weißen Blutkörperchen (RBC \& WBC) wurden dabei durch ein triangulares Netzwerk aus Teilchen erstellt.
Die Simulation konnte dabei die experimentellen Ergebnisse, dass WBC Margination bei mittleren Hämatokritwerten von $\num{0.2}$ bis $\num{0.4}$ auftreten, reproduzieren.

\paragraph{MPCD: Multi Particle Collision Dynamics:}
Die MPCD-Methode setzt sich allgemein aus zwei Schritten zusammen, dem Streaming- und dem Kollisionsschritt.
Der Streaming-Schritt besteht aus dem Euler-Verfahren, angewendet auf die ballistische Bewegung der Teilchen:
\begin{equation*}
\vec{r}_i (t + \updelta t) = \vec{r}_i (t) + \updelta t \, \vec{v}_i(t)\,.
\end{equation*}
$\vec{r}_i$ ist dabei der Ort des $i$-ten Teilchens und $\vec{v}_i$ dessen Geschwindigkeit zur angegebenen Zeit $t$.
Dagegen spielt sich die eigentliche Physik hinter dem Kollisionsschritt ab.
Dazu wird zunächst das Gefäß, in dem sich die Flüssigkeit befindet, in Gitterzellen unterteilt.
Für jede Gitterzelle wird dann der Kollisionsschritt
\begin{equation*}
\vec{v}_i (t + \updelta t)= \vec{v}_{c,\mathrm{cm}} (t) + \vec{R}_{c}\, \updelta \vec{v}_i (t) \:,
\quad \updelta \vec{v}_i(t) = \vec{v}_i(t) - \vec{v}_{c,\mathrm{cm}}(t)\,.
%&\vec{v}_{c,\mathrm{cm}}(t) = \text{mittlere Geschw. in Zelle $c$}
\end{equation*}
ausgeführt.
$\vec{v}_{c,\mathrm{cm}}$ ist dabei die mittlere Geschwindigkeit aller Teilchen in der Zelle $c$, während $\vec{R}_{c}$ eine stochastische Rotationsmatrix ist, die den relativen Geschwindigkeitsvektor $\updelta \vec{v}_i$ dreht.
Wie sich leicht zeigen lässt, ist dieser Schritt impuls- und energieerhaltend.
Zur Vermeidung von Korrelationen zwischen einzelnen Teilchen wird das Gitter zusätzlich noch in jedem Zeitschritt verschoben.
Aufgrund dieser Verschiebung entstehen am Rande des Gefäßes Zellen mit weniger Teilchen als im Mittel aller Zellen.
Diese werden daher mit sogenannten Ghost-Particles aufgefüllt.
Über Regulierung der Geschwindigkeit dieser Teilchen lässt sich diese Methode auch als Thermostat verwenden.\\
Die Einbindung eines in dem MPCD-Fluid gelösten Objekts, wie z.B. Blutkörperchen, die wie schon bei der DPD-Methode durch ein Netzwerk aus Teilchen erstellt werden können, wird über zwei Schritte geregelt.
Versucht ein Lösungsmittelteilchen in das Objekt einzudringen, so wird die Geschwindigkeit des Teilchens wie bei der No-slip-Randbedingung umgekehrt.
Zusätzlich werden die Netzwerkteilchen mit in den Kollisionsschritt eingebunden.
Dadurch gibt es zwischen dem gelösten Objekt und dem Fluid einen Impulsübertrag.
Ein Beispiel für eine Simulation, bei der RBCs (Red Blood Cells) über diese Methode in ein Fluid eingebunden wurden, wurde von Noguchi veröffentlicht \cite{noguchi12}.
Darin untersucht er das Verhalten von RBCs in einem zylindrischen Blutgefäß mit periodischen Randbedingung und einer konstanten Beschleunigung auf alle Fluidteilchen.
Zu beobachten waren dabei drei verschiedene Phasen abhängig vom Zylinderradius, dem Hämatokritwert und der Stärke der Beschleunigung: die ungeordnete-"`normale"' Phase $D$, die ausgerichtete Fallschirmphase $Pc$ und der Zigzag-slipper Phase $S$.
Diese Phasen unterscheiden sich dabei in Anordnung und Form der RBCs in dem Blutgefäß.

\paragraph{Ausblick:}
Weitere Anwendungen der beiden teilchenbasierten Simulationsmethoden zur Simulation von Blut liegen darin, erkrankte Blutzellen, wie sie z.B. bei Malaria vorkommen, zu simulieren.
%Die Simulationsergebnisse können dann bei medizinischen Technologien im Bereich der Diagnose behilflich sein.
Zudem können die Simulationen dahingehend erweitert werden, dass noch mehr Blutkörperchen oder zusätzlichen Objekte wie z.B. Krankheitserreger in den Blutfluß eingebunden werden.
Das Problem, was dabei jedoch zwangsläufig auftritt, besteht darin, dass die Rechenleistung bei einer gewissen Größe an Objekten nicht mehr ausreicht.
Eine mögliche Lösung des Problems könnte darin liegen, eine weitere Stufe des coarse-graining einzuführen und die RBCs durch schwingende "`dumbbells"' zu simulieren.
Diese Methode wurde schon erfolgreich zur Simulation einer viskoelastischen Flüssigkeit eingesetzt \cite{goetze}.

\nocite{*}
\bibliographystyle{plain}
\bibliography{Literatur.bib}
%\newpage
%\section{Anhang}
%\input{Anhang.tex}
\end{document}